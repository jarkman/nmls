# README #

Needlessly Modular Luggage System

See
http://jarkman.co.uk/catalog/luggage/nmls_alice.htm
and
http://jarkman.co.uk/catalog/luggage/nmls_julia.htm
for more

### What is this repository for? ###

Files for lasercut MOLLE bags of various sorts.

Parametric Fusion360 designs, DXFs, Lightburn project files.
